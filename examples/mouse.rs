use std::time::*;

use sketchbook_wgpu::prelude::*;

fn main() {
    Book::<Single<App>>::bind();
}

#[apply(derive_sketch)]
#[sketch(
    env = wgpu,
    aspects = (update, draw, mouse),
)]
struct App {
    #[page] page: wgpu::Page,
}

impl Setup for App {
    fn setup(mut page: wgpu::Page, _: ()) -> Self {
        page.update_rate(2.);
        Self {
            page,
        }
    }
}

impl draw::Handlers for App {
    fn draw(&mut self) {
        self.background(0x101010FF);
        self.fill(0xFF00FFFF);
        self.circle((self.mouse_x(), self.mouse_y(), 40));
    }
}

impl update::Handlers for App {
    fn update(&mut self, delta_t: Duration) {
        println!("update {:?}", delta_t);
        println!("mouse x: {}, y: {}", self.mouse_x(), self.mouse_y());
    }
}

impl mouse::Handlers for App {
    fn clicked(&mut self, &button: &MouseButton) {
        println!("mouse clicked {:?}", button);
    }
}
