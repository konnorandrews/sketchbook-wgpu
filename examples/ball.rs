use std::time::Duration;
use rand::Rng;
use sketchbook_wgpu::prelude::*;

fn main() {
    Book::<Single<App>>::bind();
}

#[apply(derive_sketch)]
#[sketch(
    env = wgpu, 
    aspects = (update, draw),
)]
struct App {
    #[page] page: Page,
    pub balls: Vec<Ball>,
}

impl Setup for App {
    fn setup(mut page: Page, _: ()) -> Self {
        page.frame_rate(75.);
        page.update_rate(120.);
        Self {
            page,
            balls: (0..1_000).map(|_| Ball::new()).collect(),
        }
    }
}

impl update::Handler for App {
    fn update(&mut self, delta_t: Duration) {
        // println!("update");

        let delta_t = delta_t.as_secs_f32();
        for ball in &mut self.balls {
            ball.update(&mut self.page, delta_t);
        }
    }
}

impl draw::Handler for App {
    fn draw(&mut self) {
        self.background(Color::new(10, 10, 10, 255));

        // println!("draw");
        self.ellipse_mode(Mode::Corner);

        // self.fill(Color { red: 0., green: 0.0, blue: 0.0, alpha: 1.0 });

        for ball in &mut self.balls {
            ball.draw(&mut self.page);
        }
    }
}

struct Ball {
    x: f32,
    y: f32,
    x_v: f32,
    y_v: f32,
    size: (f32, f32),
    fill: Color,
    // stroke: Color,
    // radius: (f32, f32, f32, f32),
    // stroke_weight: f32,
}

impl Ball {
    pub fn new() -> Self {
        let mut rng = rand::thread_rng();
        let d = rng.gen_range(100.0..300.);
        Self {
            x: 30.,
            y: 30.,
            x_v: rng.gen_range(-2.0..2.) * 50.,
            y_v: rng.gen_range(-2.0..2.) * 50.,
            // x_v: 200.,
            // y_v: 124.,
            // size: (rng.gen_range(100.0..300.), rng.gen_range(100.0..300.)),
            size: (d, d),
            // size: (20., 20.),
            // fill: palette::Srgba::from(0xFF00FFFF),
            fill: Color::new(
                rng.gen_range(0..=255),
                rng.gen_range(0..=255),
                rng.gen_range(0..=255),
                100,
            ),
        }
    }

    pub fn update(&mut self, page: &mut Page, delta_t: f32) {
        self.x = self.x + self.x_v * delta_t;
        self.y = self.y + self.y_v * delta_t;

        if self.x > page.width() as f32 - self.size.0 as f32 {
            self.x_v = -1. * self.x_v.abs();
        }

        if self.x < 0. {
            self.x_v = self.x_v.abs();
        }

        if self.y > page.height() as f32 - self.size.1 as f32 {
            self.y_v = -1. * self.y_v.abs();
        }

        if self.y < 0. {
            self.y_v = self.y_v.abs();
        }
        /*self.size += 0.5;
        if self.size > 200. {
            self.size = 0.
        }*/
    }

    pub fn draw(&self, page: &mut Page) {
        page.fill(self.fill);
        // page.rect((self.x, self.y, self.size.0, self.size.1));
        page.circle((self.x, self.y, self.size.0));
    }
}
