use sketchbook_wgpu::prelude::*;

fn main() {
    Book::<Single<App>>::bind();
}

#[apply(derive_sketch)]
#[sketch(
    env = wgpu,
    aspects = (draw, mouse)
)]
struct App {
    #[page] page: wgpu::Page,
}

impl Setup for App {
    fn setup(mut page: wgpu::Page, _: ()) -> Self {
        page.frame_rate(60.);
        Self { page }
    }
}

impl mouse::Handler for App {}

impl draw::Handler for App {
    fn draw(&mut self) {
        self.background(0x101010FF);
        self.circle((self.mouse_x(), self.mouse_y(), 20));
    }
}
