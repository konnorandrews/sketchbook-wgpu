use std::time::*;

use sketchbook_wgpu::prelude::*;

fn main() {
    Book::<Single<App>>::bind();
}

#[apply(derive_sketch)]
#[sketch(
    env = wgpu,
    aspects = (update, keyboard),
)]
struct App {
    #[page] page: wgpu::Page,
}

impl Setup for App {
    fn setup(mut page: wgpu::Page, _: ()) -> Self {
        page.update_rate(2.);
        Self {
            page,
        }
    }
}

impl update::Handler for App {
    fn update(&mut self, delta_t: Duration) {
        println!("update {:?}", delta_t);
    }
}

impl keyboard::Handler for App {
    fn pressed(&mut self, &key: &wgpu::Key) {
        println!("key pressed {:?}", key);

        if wgpu::Key::Q == key {
            self.stop();
        }
    }
}
