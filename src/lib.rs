pub mod wgpu;
pub mod fifo;
pub mod double_buffer;
pub mod color;

pub use sketchbook;

pub mod prelude {
    pub use sketchbook::*;
    pub use sketchbook::aspects::*;
    pub use sketchbook::ream::*;
    pub use sketchbook::aspects::draw::*;

    pub use sketchbook::aspects::update::SketchExt as UpdateExt;
    pub use sketchbook::aspects::draw::SketchExt as DrawExt;
    pub use sketchbook::aspects::mouse::SketchExt as MouseExt;
    pub use sketchbook::aspects::keyboard::SketchExt as KeyboardExt;

    pub use super::wgpu;
    pub use super::wgpu::Page;
    pub type Color = palette::Srgba<u8>;

    pub use winit::event::MouseButton;
}
