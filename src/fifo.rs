pub struct FiFo<T> {
    inner: fifo_impl::FiFo<T>,
}

impl<T> FiFo<T> {
    pub fn new(shared_capacity: usize) -> Self {
        Self {
            inner: fifo_impl::FiFo::new(shared_capacity),
        }
    }

    pub fn split(self) -> (FiFoPush<T>, FiFoPop<T>) {
        let (push, pop) = self.inner.split();
        (FiFoPush { inner: push }, FiFoPop { inner: pop })
    }
}

pub struct FiFoPush<T> {
    inner: fifo_impl::FiFoPush<T>,
}

pub struct FiFoPop<T> {
    inner: fifo_impl::FiFoPop<T>,
}

impl<T> FiFoPush<T> {
    pub fn push(&mut self, item: T) {
        self.inner.push(item)
    }

    pub fn try_flush(&mut self) {
        self.inner.try_flush()
    }
}

impl<T> FiFoPop<T> {
    pub fn pop(&mut self) -> Option<T> {
        self.inner.pop()
    }
}

/*
mod fifo_impl {
    struct SyncFiFo {
        inner: Rc<RefCell<VecDeque<T>>>,
    }
}
*/

mod fifo_impl {
    use std::collections::VecDeque;

    use ringbuf::{Consumer, Producer, RingBuffer};

    pub use ThreadFiFo as FiFo;
    pub use ThreadFiFoPop as FiFoPop;
    pub use ThreadFiFoPush as FiFoPush;

    pub struct ThreadFiFo<T> {
        ring_buf: RingBuffer<T>,
    }

    impl<T> ThreadFiFo<T> {
        pub fn new(shared_capacity: usize) -> Self {
            Self {
                ring_buf: RingBuffer::new(shared_capacity),
            }
        }

        pub fn split(self) -> (ThreadFiFoPush<T>, ThreadFiFoPop<T>) {
            let (producer, consumer) = self.ring_buf.split();
            (
                ThreadFiFoPush {
                    producer,
                    overflow: VecDeque::new(),
                },
                ThreadFiFoPop { consumer },
            )
        }
    }

    pub struct ThreadFiFoPush<T> {
        producer: Producer<T>,
        overflow: VecDeque<T>,
    }

    impl<T> ThreadFiFoPush<T> {
        pub fn push(&mut self, item: T) {
            self.try_flush();

            if self.producer.is_full() {
                println!("used overflow {}", self.overflow.len());
                self.overflow.push_back(item);
            } else if self.producer.push(item).is_err() {
                panic!("unexpected error pushing into producer");
            }
        }

        pub fn try_flush(&mut self) {
            while !self.producer.is_full() {
                if let Some(item) = self.overflow.pop_front() {
                    if self.producer.push(item).is_err() {
                        panic!("unexpected error pushing into producer");
                    }
                } else {
                    break
                }
            }
        }
    }

    pub struct ThreadFiFoPop<T> {
        consumer: Consumer<T>,
    }

    impl<T> ThreadFiFoPop<T> {
        pub fn pop(&mut self) -> Option<T> {
            self.consumer.pop()
        }
    }
}
