pub mod display;
pub mod run;
pub mod device;
pub mod vec_buffer;

use std::collections::HashSet;
use std::sync::mpsc::Sender;
use std::sync::Arc;
use std::sync::Mutex;
use std::time::Duration;
use std::time::Instant;

use sketchbook::aspects::draw::Shape;
use winit::event::MouseButton;
use winit::event_loop::EventLoopProxy;
use winit::window::WindowId;

use std::collections::VecDeque;

use sketchbook::aspects::*;
use sketchbook::compose;

use crate::double_buffer::*;
use crate::fifo::{FiFo, FiFoPop, FiFoPush};

use self::display::Frame;
use self::display::shape::shape_to_raw;

pub use winit::event::VirtualKeyCode as Key;
use sketchbook::compose::AsPart;
use sketchbook::apply;

pub struct EnvSpecificMarker;

#[apply(compose!)]
pub enum Events {
    #[part]
    Update(update::Event),
    #[part]
    Draw(draw::Event<EnvSpecificMarker>),
    #[part]
    Mouse(mouse::Event<EnvSpecificMarker>),
    #[part]
    Keyboard(keyboard::Event<EnvSpecificMarker>),
    Close,
}

pub struct DisplayInfo {
    width: f32,
    height: f32,
    bridge: Option<DisplayBridge>,
}

#[derive(Debug, Clone)]
pub enum DisplayEvent {
    Redraw(WindowId),
    Close(WindowId),
}

pub struct DisplayBridge {
    pub window_id: WindowId,
    pub event_loop: EventLoopProxy<DisplayEvent>,
}

#[apply(compose)]
pub struct Page {
    event_queue: FiFoPop<Events>,
    shapes: DoubleBufferWrite<Frame>,
    info: Arc<Mutex<DisplayInfo>>,
    bridge: Option<DisplayBridge>,

    #[part]
    update_data: update::EnvData<EnvSpecificMarker>,
    #[part]
    draw_data: draw::EnvData<EnvSpecificMarker>,
    #[part]
    mouse_data: mouse::EnvData<EnvSpecificMarker>,
    #[part]
    keyboard_data: keyboard::EnvData<EnvSpecificMarker>,

    update_impl: update::UpdateUsingInstant,
    last_draw: Option<Instant>,

    did_draw_this_group: bool,
    events_for_group: VecDeque<Events>,
}

struct CreateWindow {
    event_queue: FiFoPush<Events>,
    shapes: DoubleBufferRead<Frame>,
    info: Arc<Mutex<DisplayInfo>>,
}

#[derive(Clone)]
pub struct Mill {
    create_windows: Sender<CreateWindow>,
}

#[derive(Default)]
pub struct Env;

impl sketchbook::Environment for Env {
    type Mill = Mill;
    type Return = ();

    fn run<F>(self, func: F)
    where
        F: FnMut(&mut Self::Mill) -> sketchbook::Status + 'static,
    {
        run::run(self, func);
    }
}

impl sketchbook::Mill for Mill {
    type Page = Page;

    fn new_page(&mut self) -> Self::Page {
        // trigger new window
        let (event_sender, event_receiver) = FiFo::new(1000).split();
        let info = Arc::new(Mutex::new(DisplayInfo {
            width: 0.,
            height: 0.,
            bridge: None,
        }));
        let buffer = DoubleBuffer::new();
        let (read, write) = buffer.split();
        {
            self.create_windows
                .send(CreateWindow {
                    event_queue: event_sender,
                    shapes: read,
                    info: info.clone(),
                })
                .unwrap();
        }
        Page {
            did_draw_this_group: false,
            events_for_group: VecDeque::new(),
            bridge: None,
            event_queue: event_receiver,
            update_impl: update::UpdateUsingInstant::default(),
            last_draw: None,
            update_data: update::EnvData::default(),
            shapes: write,
            draw_data: draw::EnvData::default(),
            mouse_data: mouse::EnvData::default(),
            keyboard_data: keyboard::EnvData::default(),
            info,
        }
    }
}

impl update::EnvPageExt<EnvSpecificMarker> for Page {
    fn get_time(&mut self) -> Instant {
        Instant::now()
    }
}

impl keyboard::EnvSpecific for EnvSpecificMarker {
    type Key = Key;
    type KeySet = HashSet<Key>;
}

impl keyboard::AssociatedEnvSpecificMarker for Env {
    type EnvSpecificMarker = EnvSpecificMarker;
}

impl update::EnvSpecific for EnvSpecificMarker {
    type Time = Instant;
    type Duration = Duration;
    type Rate = f32;

    fn default_update_rate() -> Self::Rate {
        30.0
    }

    fn zero_duration() -> Self::Duration {
        Duration::from_millis(0)    
    }

    fn duration_between(start: &Self::Time, end: &Self::Time) -> Self::Duration {
        end.duration_since(*start)
    }
}

impl update::AssociatedEnvSpecificMarker for Env {
    type EnvSpecificMarker = EnvSpecificMarker;
}

impl draw::EnvSpecific for EnvSpecificMarker {
    type Num = f32;
    type Color = palette::Srgba<u8>;
    type ShapeBuffer = Vec<Shape<Self::Num, Self::Color>>;

    fn default_size() -> sketchbook::Size2<Self::Num> {
        sketchbook::Size2 { width: 1.0, height: 1.0 }    
    }

    fn default_context() -> draw::Context<Self::Num, Self::Color> {
        draw::Context {
            fill: palette::Srgba::new(255, 255, 255, 255),
            stroke: palette::Srgba::new(0, 0, 0, 255),
            weight: 0.0,
            rectangle_mode: draw::Mode::Center,
            ellipse_mode: draw::Mode::Center,
            rotation: 0.0,
        }
    }

    fn default_background() -> Self::Color {
        palette::Srgba::new(10, 10, 10, 255)
    }

    fn default_frame_rate() -> Self::Num {
        30.0
    }
}

impl draw::AssociatedEnvSpecificMarker for Env {
    type EnvSpecificMarker = EnvSpecificMarker;
}

impl mouse::EnvSpecific for EnvSpecificMarker {
    type Button = MouseButton;
    type WheelCount = f32;
    type Position = f32;
    type ButtonSet = HashSet<MouseButton>;
}

impl mouse::AssociatedEnvSpecificMarker for Env {
    type EnvSpecificMarker = EnvSpecificMarker;
}

impl sketchbook::Page for Page {
    type Event = Events;

    fn start_group(&mut self) {
        // sync with the env state
        // the env state may change while this group is being processed
        // however, this page will consider it to stay the same to reduce locking

        let (width, height) = {
            let mut info = self.info.lock().unwrap();
            if let Some(bridge) = info.bridge.take() {
                self.bridge = Some(bridge);
            }
            (info.width, info.height)
        };
        self.draw_data.set_size(sketchbook::Size2 { width, height });

        // load events to be processed for this group
        // load up to set limit of events from the event queue
        // this limit exists because the main thread could be continuously pushing events
        for _ in 0..100 {
            if let Some(event) = self.event_queue.pop() {
                self.events_for_group.push_back(event);
            } else {
                break;
            }
        }

        // add event for update and draw if needed
        // draw events are setup to happen after update events
        // update/draw events also happen after all env events have happened

        if self.update_impl.should_update(*self.update_data.update_rate()) {
            self.events_for_group.push_back(Events::Update(update::Event::Update));
        }

        if let Some(last_draw) = self.last_draw {
            if last_draw.elapsed().as_secs_f32() >= 1.0 / self.draw_data.frame_rate() {
                self.last_draw = Some(Instant::now());
                self.events_for_group.push_back(Events::Draw(draw::Event::Draw));
            }
        } else {
            self.last_draw = Some(Instant::now());
            self.events_for_group.push_back(Events::Draw(draw::Event::Draw));
        }

        // no draw events have been processed yet
        self.did_draw_this_group = false;
    }

    fn end_group(&mut self) {
        if !self.events_for_group.is_empty() {
            eprintln!("Event buffer was not completely processed. Events will roll over to next group.");
        }

        // write draw buffer to the double buffer and swap if draw event happend
        if self.did_draw_this_group {
            let background = self.draw_data.background_color();
            self.shapes.background = wgpu::Color {
                r: background.red as f64 / 255.0,
                g: background.green as f64 / 255.0,
                b: background.blue as f64 / 255.0,
                a: background.alpha as f64 / 255.0,
            };

            // empty double buffer before re-use
            self.shapes.buffer.clear();

            // fill double buffer with shape data
            self.shapes.buffer.extend(self.draw_data.frame_mut().iter().map(|shape| shape_to_raw(shape)));

            // check for run away drawing
            if self.draw_data.frame_mut().len() > 1_000_000 {
                eprintln!("Frame buffer has {} shapes.", self.draw_data.frame_mut().len())
            }

            // swap buffer with main thread for rendering
            DoubleBufferWrite::swap(&mut self.shapes);

            // signal a user redraw has happend
            if let Some(bridge) = &self.bridge {
                bridge
                    .event_loop
                    .send_event(DisplayEvent::Redraw(bridge.window_id))
                    .ok();
            }
        }
    }

    fn next_event_in_group(&mut self) -> Option<Self::Event> {
        self.events_for_group.pop_front()
    }
    
    fn before_event(&mut self, event: &Self::Event) {
        // this page doesn't require any pre processing of events
    }

    fn finish_event(&mut self, event: Self::Event) {
        match event {
            Events::Draw(_) => {
                // flag that a draw event happened this group
                self.did_draw_this_group = true;
            },
            Events::Close => {
                // flag that the sketch should stop
                self.update_data.stop();
            },
            _ => {},
        }
    }

    fn status(&self) -> sketchbook::Status {
        if self.update_data.should_stop() {
            sketchbook::Status::Stop
        } else {
            sketchbook::Status::Continue
        }
    }
}

impl Drop for Page {
    fn drop(&mut self) {
        // NOTE: there is the possibility of a data race here if the bridge has not been setup yet
        if let Some(bridge) = &self.bridge {
            bridge
                .event_loop
                .send_event(DisplayEvent::Close(bridge.window_id))
                .ok();
        }
    }
}
