use std::borrow::Cow;
use std::collections::HashMap;
use std::sync::mpsc::{channel, TryRecvError};

use winit::event::Event;
use winit::event_loop::{ControlFlow, EventLoop};

use crate::wgpu::display::Display;
use crate::wgpu::DisplayBridge;

use super::Mill;
use super::{DisplayEvent, Env};
use std::time::{self, Instant};
use winit::window::WindowId;

use wgpu::{Adapter, Device, Instance, Queue, ShaderModule};

// const WAIT_TIME: time::Duration = time::Duration::from_millis(15);
const WAIT_TIME: time::Duration = time::Duration::from_millis(15);

pub fn run<F: 'static>(_env: Env, mut func: F)
where
    F: FnMut(&mut Mill) -> sketchbook::Status,
{
    let event_loop: EventLoop<DisplayEvent> = EventLoop::with_user_event();
    env_logger::init();

    let mut windows: HashMap<WindowId, Display> = HashMap::new();
    let mut window_last_redraw: HashMap<WindowId, Instant> = HashMap::new();
    let (create_window_sender, create_window_receiver) = channel();
    let mut mill = Mill {
        create_windows: create_window_sender,
    };

    let instance = wgpu::Instance::new(wgpu::Backends::all());

    let proxy = event_loop.create_proxy();

    let mut status = sketchbook::Status::Continue;

    let mut batch_start = time::Instant::now();
    event_loop.run(move |event, window_target, control_flow| {
        match &event {
            Event::NewEvents(_start_cause) => {
                // record instant for control flow
                batch_start = time::Instant::now();
            }
            Event::WindowEvent { event, window_id } => {
                if let Some(display) = windows.get_mut(&window_id) {
                    display.handle_window_event(event);
                }
            }
            Event::MainEventsCleared => {
                // update ream
                status = func(&mut mill);

                // with main events cleared check if any new windows need to be created
                loop {
                    match create_window_receiver.try_recv() {
                        Ok(create) => {
                            let window = winit::window::Window::new(window_target).unwrap();

                            {
                                let mut info = create.info.lock().unwrap();
                                info.bridge = Some(DisplayBridge {
                                    window_id: window.id(),
                                    event_loop: proxy.clone(),
                                });
                            }

                            window_last_redraw.insert(window.id(), Instant::now());
                            windows.insert(
                                window.id(),
                                Display::new(
                                    &instance,
                                    window,
                                    create.event_queue,
                                    create.shapes,
                                    create.info,
                                ),
                            );
                            // println!("created window");
                        }
                        Err(TryRecvError::Empty) => break,
                        _ => panic!("channel broken"),
                    }
                }
            }
            Event::RedrawRequested(window_id) => {
                // redraw requested window
                if let Some(last) = window_last_redraw.get_mut(&window_id) {
                    if last.elapsed().as_millis() > 1 {
                        if let Some(display) = windows.get_mut(&window_id) {
                            *last = Instant::now();
                            display.redraw();
                            // let now = Instant::now();
                            // println!("fps {}", 1./now.duration_since(last_redraw).as_secs_f32());
                            // last_redraw = now;
                        }
                        
                    }
                }
            }
            Event::RedrawEventsCleared => {
                if let sketchbook::Status::Stop = status {
                    println!("clean exit from winit");
                    *control_flow = ControlFlow::Exit
                } else {
                    // wait for given time unless event happens
                    *control_flow = ControlFlow::WaitUntil(batch_start + WAIT_TIME);
                    // *control_flow = ControlFlow::Poll;
                }
            }
            Event::UserEvent(DisplayEvent::Redraw(window_id)) => {
                if let Some(last) = window_last_redraw.get_mut(&window_id) {
                    if last.elapsed().as_millis() > 1 {
                        // request redraw for user requested window
                        if let Some(display) = windows.get_mut(&window_id) {
                            // display.window_request_redraw();
                            *last = Instant::now();
                            display.redraw()
                        }
                    }
                }
            }
            Event::UserEvent(DisplayEvent::Close(window_id)) => {
                windows.remove(&window_id);
            }
            _ => {}
        }
    });
}
