use std::marker::PhantomData;
use std::time::Instant;

use bytemuck::Pod;

use wgpu::util::DeviceExt;
use wgpu::{Buffer, BufferSlice, BufferUsages, Device, Queue};

pub struct VecBuffer<T> {
    buffer_capacity: usize,
    buffer_len: usize,
    gpu_buffer: Buffer,
    marker: PhantomData<T>,
    usage: BufferUsages,
}

impl<T> VecBuffer<T>
where
    T: Pod,
{
    pub fn new(device: &Device, usage: BufferUsages) -> Self {
        let gpu_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Vec Buffer"),
            contents: &[],
            usage,
        });

        Self {
            gpu_buffer,
            buffer_capacity: 0,
            buffer_len: 0,
            usage,
            marker: PhantomData,
        }
    }

    fn needs_alloc(&self, data: &[T]) -> bool {
        self.buffer_capacity < data.len()
    }

    fn alloc_buffer(&mut self, device: &Device, data: &[T]) {
        // destroy previous buffer
        self.gpu_buffer.destroy();

        println!("buffer alloc {}", data.len());

        self.buffer_capacity = data.len();
        self.buffer_len = data.len();

        // create a new buffer
        self.gpu_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Pool Buffer"),
            contents: bytemuck::cast_slice(data),
            usage: self.usage,
        });
    }

    pub fn slice(&self) -> BufferSlice<'_> {
        self.gpu_buffer.slice(..)
    }

    pub fn load_data(&mut self, device: &Device, queue: &Queue, data: &[T]) {
        if self.needs_alloc(data) {
            self.alloc_buffer(device, data);
        } else {
            queue.write_buffer(&self.gpu_buffer, 0, bytemuck::cast_slice(data));
            self.buffer_len = data.len();
        }
    }

    pub fn len(&self) -> usize {
        self.buffer_len
    }

    pub fn is_empty(&self) -> bool {
        self.buffer_len == 0
    }
}
