use winit::window::Window;

#[derive(Debug)]
pub struct WgpuSurface {
    // the order of these fields is important for unsafe code
    // the surface must be dropped first, then the window
    surface: wgpu::Surface,
    window: Window,
}

impl WgpuSurface {
    pub fn for_window(instance: &wgpu::Instance, window: Window) -> Self {
        let surface = unsafe { instance.create_surface(&window) };

        Self { surface, window }
    }

    pub fn best_adapter<F, B>(self, instance: &wgpu::Instance, compare: F) -> Result<WgpuAdapter, WgpuSurface>
    where
        B: Ord,
        F: Fn(&wgpu::Adapter) -> B,
    {
        if let Some(adapter) = instance.enumerate_adapters(wgpu::Backends::all()).filter(|adapter| {
            adapter.is_surface_supported(&self.surface)
        }).max_by_key(compare) {
            Ok(WgpuAdapter {
                adapter,
                surface: self,
            })
        } else {
            Err(self)
        }
    }

    pub fn prefer_low_power(adapter: &wgpu::Adapter) -> u8 {
        match adapter.get_info().device_type {
            wgpu::DeviceType::IntegratedGpu => 5,
            wgpu::DeviceType::VirtualGpu => 4,
            wgpu::DeviceType::DiscreteGpu => 3,
            wgpu::DeviceType::Cpu => 2,
            wgpu::DeviceType::Other => 1,
        }
    }

    pub fn prefer_high_power(adapter: &wgpu::Adapter) -> u8 {
        match adapter.get_info().device_type {
            wgpu::DeviceType::DiscreteGpu => 5,
            wgpu::DeviceType::VirtualGpu => 4,
            wgpu::DeviceType::IntegratedGpu => 3,
            wgpu::DeviceType::Cpu => 2,
            wgpu::DeviceType::Other => 1,
        }
    }
}

#[derive(Debug)]
pub struct WgpuAdapter {
    pub(crate) adapter: wgpu::Adapter,
    surface: WgpuSurface,
}

impl WgpuAdapter {
    pub async fn create_device(self) -> Result<WgpuDevice, (WgpuAdapter, wgpu::RequestDeviceError)> {
        let result = self.adapter.request_device(
            &wgpu::DeviceDescriptor {
                features: wgpu::Features::empty(),
                limits: wgpu::Limits::default(),
                label: None,
            },
            None
        ).await;

        match result {
            Ok((device, queue)) => {
                let size = self.surface.window.inner_size();
                if size.width == 0 || size.height == 0 {
                    panic!("Window size is 0. Cannot create device.");
                }

                let config = wgpu::SurfaceConfiguration {
                    usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
                    format: self.surface.surface.get_preferred_format(&self.adapter).unwrap(),
                    width: size.width,
                    height: size.height,
                    present_mode: wgpu::PresentMode::Fifo,
                };

                self.surface.surface.configure(&device, &config);

                Ok(WgpuDevice {
                    adapter: self,
                    device,
                    queue,
                    config,
                })
            }
            Err(error) => Err((self, error))
        }
    }
}

#[derive(Debug)]
pub struct WgpuDevice {
    pub device: wgpu::Device,
    pub queue: wgpu::Queue,
    adapter: WgpuAdapter,
    config: wgpu::SurfaceConfiguration,
}

impl WgpuDevice {
    pub fn resize(&mut self, size: winit::dpi::PhysicalSize<u32>) {
        self.config.width = size.width;
        self.config.height = size.height;
        self.adapter.surface.surface.configure(&self.device, &self.config);
    }

    pub fn request_redraw(&self) {
        self.adapter.surface.window.request_redraw()
    }

    pub fn inner_size(&self) -> winit::dpi::PhysicalSize<u32> {
        self.adapter.surface.window.inner_size()
    }

    pub fn scale_factor(&self) -> f64 {
        self.adapter.surface.window.scale_factor()
    }

    pub fn redraw<F>(&mut self, render: F) -> Result<(), wgpu::SurfaceError>
    where
        F: FnOnce(&wgpu::TextureView, &mut wgpu::CommandEncoder)
    {
        match self.adapter.surface.surface.get_current_texture() {
            Ok(output) => {
                let view = output.texture.create_view(&wgpu::TextureViewDescriptor::default());

                let mut encoder = self.device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
                    label: Some("Render Encoder"),
                });

                render(&view, &mut encoder);

                // submit will accept anything that implements IntoIter
                self.queue.submit(std::iter::once(encoder.finish()));
                output.present();

                Ok(())
            }   
            // Reconfigure the surface if lost
            Err(err @ wgpu::SurfaceError::Lost) => {
                // self.resize(self.inner_size());
                Err(err)
            },
            Err(err @ wgpu::SurfaceError::Outdated) => {
                // self.resize(self.inner_size());
                Err(err)
            },
            // The system is out of memory, we should probably quit
            Err(err @ wgpu::SurfaceError::OutOfMemory) => {
                eprintln!("Out of memory. Unable to create render buffer.");
                Err(err)
            },
            // All other errors (Outdated, Timeout) should be resolved by the next frame
            Err(e) => {
                eprintln!("{:?}", e);
                Err(e)
            }
        }
    }

    pub fn create_pipeline(&mut self, shader_source: &str, bind_group_layouts: &[&wgpu::BindGroupLayout], buffers: &[wgpu::VertexBufferLayout]) -> wgpu::RenderPipeline {
        let shader = self.device.create_shader_module(&wgpu::ShaderModuleDescriptor {
            label: Some("Shader"),
            source: wgpu::ShaderSource::Wgsl(shader_source.into()),
        });

        let render_pipeline_layout = self.device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("Render Pipeline Layout"),
            bind_group_layouts,
            push_constant_ranges: &[],
        });

        let render_pipeline = self.device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "v_main",
                buffers, 
            },
            fragment: Some(wgpu::FragmentState { 
                module: &shader,
                entry_point: "f_main",
                targets: &[wgpu::ColorTargetState { 
                    format: self.config.format,
                    blend: Some(wgpu::BlendState::ALPHA_BLENDING),
                    write_mask: wgpu::ColorWrites::ALL,
                }],
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleStrip,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Cw,
                cull_mode: Some(wgpu::Face::Back),
                polygon_mode: wgpu::PolygonMode::Fill,
                unclipped_depth: false,
                conservative: false,
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        });

        render_pipeline
    }
}
