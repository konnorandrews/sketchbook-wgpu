// Vertex shader

struct Uniform {
    size: vec2<f32>;
    scale: f32;
};

[[group(0), binding(0)]]
var<uniform> surface: Uniform;

struct VertexInput {
    [[location(0)]] position: vec2<f32>;
};

struct ShapeInput {
    [[location(1)]] center: vec2<f32>;
    [[location(2)]] size: vec2<f32>;
    [[location(3)]] weight_rotation: u32;
    [[location(4)]] fill: u32;
    [[location(5)]] stroke: u32;
    [[location(6)]] primitive_other: u32;
};

struct VertexOutput {
    [[builtin(position)]] clip_position: vec4<f32>;
    [[location(0)]] uv: vec2<f32>;
    [[location(1)]] scale: f32;

    [[location(2)]] primitive: u32;
    [[location(3)]] size: vec2<f32>;
    [[location(4)]] fill: vec4<f32>;
    [[location(5)]] stroke: vec4<f32>;
    [[location(6)]] weight: f32;
    [[location(7)]] rotation: f32;
};

fn rot_2d(angle: f32) -> mat2x2<f32> {
    let s = sin(angle);
    let c = cos(angle);
    return mat2x2<f32>(vec2<f32>(c, -s), vec2<f32>(s, c));
}

[[stage(vertex)]]
fn v_main(
    model: VertexInput,
    shape: ShapeInput,
) -> VertexOutput {
    var out: VertexOutput;

    let e = surface.scale * 6.0;

    let weight_rotation = unpack2x16float(shape.weight_rotation);
    let primitive_other = unpack4x8unorm(shape.primitive_other);
    let fill = unpack4x8unorm(shape.fill);
    let stroke = unpack4x8unorm(shape.stroke);

    let position = rot_2d(weight_rotation.y) * (model.position * (shape.size + e)/2.);

    let corner_pixel = position + shape.center;
    let corner_unit = (corner_pixel / surface.size) * vec2<f32>(2.,-2.) - vec2<f32>(1.,-1.);
    out.clip_position = vec4<f32>(corner_unit, 0.0, 1.0);

    out.uv = model.position * (shape.size + e)/2.;

    out.scale = surface.scale;
    out.primitive = u32(primitive_other.x);
    out.size = shape.size;
    out.fill = fill;
    out.stroke = stroke;
    out.weight = weight_rotation.x;
    out.rotation = weight_rotation.y;

    return out;
}

// Fragment shader

fn circle(point: vec2<f32>, radius: f32) -> f32 {
    return length(point) - radius;
}

fn rect(point: vec2<f32>, size: vec2<f32>) -> f32 {
    let d = abs(point) - size/2.;
    return length(max(d, vec2<f32>(0., 0.))) + min(max(d.x, d.y), 0.0);
}

[[stage(fragment)]]
fn f_main(in: VertexOutput) -> [[location(0)]] vec4<f32> {
    let s = 1./in.scale;
    // let s = 1./in.scale * 5.0;

    var d: f32;

    if (in.primitive == 0u) {
        d = circle(in.uv, in.size.x/2.0);
    } else if (in.primitive == 1u) {
        d = rect(in.uv, in.size);
    }

    var c = in.fill;
    if (d < -s/2.) {

    } else if (d > s/2.) {
        c.w = 0.0;
    } else {
        let w = smoothStep(s/2., -s/2., d);
        c.w = c.w * w;
    }
    return c;
}
