use bytemuck::{Pod, Zeroable};
use half::f16;
use std::mem;
use wgpu::{vertex_attr_array, VertexAttribute};

use sketchbook::aspects::draw::Shape;

#[repr(C)]
#[derive(Copy, Clone, Pod, Zeroable)]
pub struct ShapeRaw {
    center: [f32; 2],
    size: [f32; 2],
    weight: f16,
    rotation: f16,
    fill: [u8; 4],
    stroke: [u8; 4],
    primitive: u8,
    other: [u8; 3],
}

const _: () = {
    if std::mem::size_of::<ShapeRaw>() != 32 {
        panic!("Shape size is wrong");
    }
};

const SHAPE_RAW_ATTR: [VertexAttribute; 6] = vertex_attr_array![
    1 => Float32x2,
    2 => Float32x2,
    3 => Uint32,
    4 => Uint32,
    5 => Uint32,
    6 => Uint32,
];

fn color_to_raw(color: palette::Srgba<u8>) -> [u8; 4] {
    [color.red, color.green, color.blue, color.alpha]
}

pub fn shape_to_raw(shape: &Shape<f32, palette::Srgba<u8>>) -> ShapeRaw {
    use Shape::*;
    match shape {
        Circle(circle) => ShapeRaw {
            primitive: 0,
            center: [circle.center.x, circle.center.y],
            size: [circle.diameter, circle.diameter],
            fill: color_to_raw(circle.fill),
            stroke: color_to_raw(circle.stroke),
            weight: f16::from_f32(circle.weight),
            rotation: f16::from_f32(0.),
            other: [0; 3],
        },
        Rectangle(rectangle) => ShapeRaw {
            primitive: 1,
            center: [rectangle.center.x, rectangle.center.y],
            size: [rectangle.size.width, rectangle.size.height],
            fill: color_to_raw(rectangle.fill),
            stroke: color_to_raw(rectangle.stroke),
            weight: f16::from_f32(rectangle.weight),
            rotation: f16::from_f32(rectangle.rotation),
            other: [0; 3],
        },
    }
}

impl ShapeRaw {
    pub fn description<'a>() -> wgpu::VertexBufferLayout<'a> {
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<Self>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Instance,
            attributes: &SHAPE_RAW_ATTR,
        }
    }
}
