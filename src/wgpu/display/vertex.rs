use bytemuck::{Pod, Zeroable};
use wgpu::{vertex_attr_array, VertexAttribute};

pub struct Vertex {
    pub x: f32,
    pub y: f32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
pub struct VertexRaw {
    position: [f32; 2],
}

const VERTEX_RAW_ATTR: [VertexAttribute; 1] = vertex_attr_array![
    0 => Float32x2,
];

impl VertexRaw {
    pub fn description<'a>() -> wgpu::VertexBufferLayout<'a> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Self>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &VERTEX_RAW_ATTR,
        }
    }
}

impl Vertex {
    pub const fn to_raw(&self) -> VertexRaw {
        VertexRaw {
            position: [self.x, self.y],
        }
    }
}
