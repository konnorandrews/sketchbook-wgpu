use bytemuck::{Pod, Zeroable};
use winit::dpi::PhysicalSize;

pub struct Uniform {
    pub surface_size: PhysicalSize<u32>,
    pub scale: f64,
}

#[repr(C)]
#[derive(Debug, Copy, Clone, Pod, Zeroable)]
pub struct UniformRaw {
    surface_size: [f32; 2],
    scale: f32,
    _extra: f32,
}

impl Uniform {
    pub fn to_raw(&self) -> UniformRaw {
        let logical = self.surface_size.to_logical(self.scale);
        UniformRaw {
            surface_size: [logical.width, logical.height],
            scale: self.scale as f32,
            _extra: 0.0,
        }
    }
}
