pub mod shape;
pub mod uniform;
pub mod vertex;

use std::collections::HashMap;
use std::sync::Arc;
use std::sync::Mutex;
use sketchbook::aspects::*;
use sketchbook::Point2;
use wgpu::util::DeviceExt;
use winit::event::VirtualKeyCode;

use wgpu::RenderPipeline;
use wgpu::Surface;
use wgpu::SurfaceConfiguration;
use wgpu::{Device, Queue};
use winit::dpi::{PhysicalPosition, PhysicalSize};
use winit::event::ElementState;
use winit::event::MouseButton;
use winit::event::WindowEvent;
use winit::window::Window;

use crate::fifo::FiFoPush;

use super::vec_buffer::VecBuffer;
use super::{DisplayInfo, Events};
use std::time::Instant;

use shape::*;
use uniform::*;
use vertex::*;

use crate::double_buffer::*;
use super::device::*;

#[derive(Default)]
pub struct Frame {
    pub background: wgpu::Color,
    pub buffer: Vec<ShapeRaw>,
}

pub struct Display {
    event_queue: FiFoPush<Events>,
    frame: DoubleBufferRead<Frame>,
    info: Arc<Mutex<DisplayInfo>>,

    shape_buffer: VecBuffer<ShapeRaw>,
    background: wgpu::Color,

    device: WgpuDevice,
    render_pipeline: RenderPipeline,

    vertex_buffer: wgpu::Buffer,
    screen_buffer: wgpu::Buffer,
    screen_bind_group: wgpu::BindGroup,
    index_buffer: wgpu::Buffer,
}

const VERTICES: &[VertexRaw] = &[
    Vertex { x: 1., y: 1. }.to_raw(),
    Vertex { x: -1., y: 1. }.to_raw(),
    Vertex { x: 1., y: -1. }.to_raw(),
    Vertex { x: -1., y: -1. }.to_raw(),
];

const INDICES: &[u16] = &[0, 1, 2, 1, 2, 3];

impl Display {
    pub fn new(
        instance: &wgpu::Instance,
        window: Window,
        event_queue: FiFoPush<Events>,
        frame: DoubleBufferRead<Frame>,
        info: Arc<Mutex<DisplayInfo>>,
    ) -> Display {
        let surface = WgpuSurface::for_window(instance, window);
        let adapter = surface.best_adapter(instance, WgpuSurface::prefer_high_power).expect("Failed to get adapter.");
        println!("Adapter: {:?}", adapter.adapter.get_info());
        let mut device = pollster::block_on(adapter.create_device()).expect("Failed to get device.");

        let vertex_buffer = device.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Vertex Buffer"),
            contents: bytemuck::cast_slice(VERTICES),
            usage: wgpu::BufferUsages::VERTEX,
        });

        let index_buffer = device.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Index Buffer"),
            contents: bytemuck::cast_slice(INDICES),
            usage: wgpu::BufferUsages::INDEX,
        });

        let shape_buffer = VecBuffer::new(
            &device.device,
            wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
        );

        let screen_uniform = Uniform {
            surface_size: device.inner_size(),
            scale: device.scale_factor(),
        }
        .to_raw();
        let screen_buffer = device.device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Screen Buffer"),
            contents: bytemuck::cast_slice(&[screen_uniform]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });

        let screen_bind_group_layout =
            device.device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
                label: Some("screen_bind_group_layout"),
            });

        let screen_bind_group = device.device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &screen_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: screen_buffer.as_entire_binding(),
            }],
            label: Some("screen_bind_group"),
        });

        let render_pipeline = device.create_pipeline(
            include_str!("shader.wgsl"),
            &[&screen_bind_group_layout],
            &[VertexRaw::description(), ShapeRaw::description()],
        );

        Self {
            vertex_buffer,
            screen_buffer,
            screen_bind_group,
            index_buffer,
            frame,
            event_queue,
            info,
            device,
            render_pipeline,
            shape_buffer,
            background: wgpu::Color { r: 0.1, g: 0.2, b: 0.3, a: 1.0 },
        }
    }

    pub fn trigger_draw(&mut self) {
        self.event_queue.push(Events::Draw(draw::Event::Draw));
    }

    pub fn mouse_move(&mut self, position: PhysicalPosition<f64>) {
        let position = position.to_logical(self.device.scale_factor());
        self.event_queue.push(Events::Mouse(mouse::Event::MoveAbsolute(Point2 {
            x: position.x,
            y: position.y,
        })));
    }

    pub fn mouse_input(&mut self, state: ElementState, button: MouseButton) {
        let event = match (state, button) {
            (ElementState::Pressed, button) => {
                mouse::Event::Press(button)
            }
            (ElementState::Released, button) => {
                mouse::Event::Release(button)
            }
        };
        self.event_queue.push(Events::Mouse(event));
    }

    pub fn keyboard_input(&mut self, keycode: VirtualKeyCode, state: ElementState) {
        match state {
            ElementState::Pressed => {
                self.event_queue
                    .push(Events::Keyboard(keyboard::Event::Press(keycode)));
            }
            ElementState::Released => {
                self.event_queue
                    .push(Events::Keyboard(keyboard::Event::Release(keycode)));
            }
        }
    }

    pub fn close(&mut self) {
        self.event_queue.push(Events::Close);
    }

    pub fn window_request_redraw(&mut self) {
        self.device.request_redraw();
    }

    pub fn handle_window_event(
        &mut self,
        event: &WindowEvent<'_>,
    ) {
        match event {
            WindowEvent::Resized(size) => {
                self.resize(*size);
            }
            WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                self.resize(**new_inner_size);
            }
            WindowEvent::CursorMoved { position, .. } => {
                self.mouse_move(*position);
            }
            WindowEvent::MouseInput { state, button, .. } => {
                self.mouse_input(*state, *button);
            }
            WindowEvent::KeyboardInput { input, .. } => {
                if let Some(keycode) = input.virtual_keycode {
                    self.keyboard_input(keycode, input.state);
                }
            }
            WindowEvent::CloseRequested => {
                self.close();
            }
            _ => {}
        }
    }

    pub fn resize(&mut self, size: PhysicalSize<u32>) {
        self.device.resize(size);

        {
            let mut info = self.info.lock().unwrap();
            let size = size.to_logical(self.device.scale_factor());
            info.width = size.width;
            info.height = size.height;
        }

        self.trigger_draw();

        self.device.queue.write_buffer(
            &self.screen_buffer,
            0,
            bytemuck::cast_slice(&[Uniform {
                surface_size: size,
                scale: self.device.scale_factor(),
            }
            .to_raw()]),
        );
    }

    fn update_draw_buffer(&mut self) {
        if let Some(frame) = self.frame.if_changed() {
            self.background = frame.background;
            self.shape_buffer.load_data(&self.device.device, &self.device.queue, &frame.buffer);
        }
    }

    pub fn redraw(&mut self) {
        self.update_draw_buffer();
        
        let result = self.device.redraw(|view, encoder| {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Render Pass"),
                color_attachments: &[wgpu::RenderPassColorAttachment {
                    view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(self.background),
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&self.render_pipeline);
            render_pass.set_bind_group(0, &self.screen_bind_group, &[]);
            render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
            render_pass.set_vertex_buffer(1, self.shape_buffer.slice());
            render_pass.set_index_buffer(self.index_buffer.slice(..), wgpu::IndexFormat::Uint16);
            render_pass.draw_indexed(
                0..INDICES.len() as u32,
                0,
                0..self.shape_buffer.len() as u32,
            );
        });

        match result {
            Err(wgpu::SurfaceError::Outdated | wgpu::SurfaceError::Lost) => {
                // fix surface getting out of sync with window
                self.resize(self.device.inner_size());
            }
            _ => {}
        }
    }
}
