pub struct Color(palette::Srgba<u8>);

impl From<(u8, u8, u8)> for Color {
    fn from(color: (u8, u8, u8)) -> Self {
        let (red, green, blue) = color;
        Self(palette::Srgba::new(red, green, blue, 255))
    }
}

impl From<(u8, u8, u8, u8)> for Color {
    fn from(color: (u8, u8, u8, u8)) -> Self {
        let (red, green, blue, alpha) = color;
        Self(palette::Srgba::new(red, green, blue, alpha))
    }
}

impl From<(f32, f32, f32)> for Color {
    fn from(color: (f32, f32, f32)) -> Self {
        let (red, green, blue) = color;
        Self(palette::Srgba::new((red * 255.0) as u8, (green * 255.0) as u8, (blue * 255.0) as u8, 255))
    }
}
